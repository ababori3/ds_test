package listTest;

import java.util.ArrayList;

public class Main {

	void start() {
		ListLinked test = new ListLinked();

		test.add(9);
		test.add(9);
		test.add(9);
		test.add(9);
		test.add(6);
		test.add(5);
		test.remove(6);
		test.remove(5);
//		test.remove(1);

//		test.add(5);
//		test.add(4);
//		test.add(2);
//		test.add(6);
//		test.add(8);

		ArrayList<Integer> result = test.toArrayList();

		for (int i = 0; i < result.size(); i++) {
			System.out.printf("%d", result.get(i));
		}

	}

	public static void main(String[] args) {
		new Main().start();
	}

}
