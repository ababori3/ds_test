package cmtest;


public class TreeNode {
	
	int data;
	TreeNode leftNode = null;
	TreeNode rightNode = null;
	
	TreeNode(int t){
		this.data = t;
	}
	
}