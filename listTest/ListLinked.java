package listTest;

import java.util.ArrayList;

public class ListLinked {

	Element head;

	public void add(int t) {

		if (head == null) {
			head = new Element(t);
		} else {
			Element current = head;
			while (current.next != null) {
				current = current.next;
			}
			current.next = new Element(t);
		}

		// throw new UnsupportedOperationException();
	}

	public void remove(int t) {

		if (head.next == null) {
			if (t == head.data) {
				head = null;
			}
		}

		else if (t == head.data) {
//			head.data = null;
			head = head.next;
		} else {

			Element current = head.next;

			while (current.next.data != t && current.next != null) {
//				if (current.next == null) {
//					return;
//				}
				current = current.next;
			}

			if(current.next !=null) {
				if (current.data == t) {
//					current.next.data = null;
					current.next = current.next.next;
				}
				
			}

		}

		// throw new UnsupportedOperationException();
	}

	public ArrayList<Integer> toArrayList() {

		ArrayList<Integer> result = new ArrayList<Integer>();
		Element current = head;
		while (current != null) {
			result.add(current.data);
			current = current.next;
		}
		return result;

		// throw new UnsupportedOperationException();
	}
}