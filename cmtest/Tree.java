package cmtest;

import java.util.ArrayList;

public class Tree {

	ArrayList<Integer> result;
	TreeNode root = null;

	public void add(int t) {

		TreeNode result = new TreeNode(t);

		if (root == null) {
			root = result;
		} else {
			TreeNode parent = null;
			TreeNode current = root;
			boolean finished = false;
			while (finished == false) {
				parent = current;
				if (t <= current.data) {
					current = current.leftNode;
					if (current == null) {
						parent.leftNode = result;
						finished = true;
					}
				} else if (t > current.data) {
					current = current.rightNode;
					if (current == null) {
						parent.rightNode = result;
						finished = true;
					}
				}
			}
		}

		// throw new UnsupportedOperationException();
	}

	public void remove(int t) {

		TreeNode current = root;
		TreeNode parent = root;
		boolean leftNode = false;

		if (root == null) {
			return;
		}

		while (t != current.data && current != null) {
			if (t < current.data) {
				parent = current;
				current = current.leftNode;
				leftNode = true;
			}
			if (t > current.data) {
				parent = current;
				current = current.rightNode;
				leftNode = false;
			}

		}
		if (t == current.data) {

			if (noChildren(current)) {
				removeChildlessNode(current, parent, leftNode);
			} else if (leftChild(current)) {
				removeLeftChildNode(current, parent, leftNode);
			} else if (rightChild(current)) {
				removeRightChildNode(current, parent, leftNode);
			} else if (twoChildren(current)) {
				removeTwoChildNode(current, parent, leftNode);
			}
		}

		// throw new UnsupportedOperationException();
	}

	boolean noChildren(TreeNode input) {
		return input.leftNode == null && input.rightNode == null;
	}

	boolean leftChild(TreeNode input) {
		return (input.leftNode != null && input.rightNode == null);
	}

	boolean rightChild(TreeNode input) {
		return input.leftNode == null && input.rightNode != null;
	}

	boolean twoChildren(TreeNode input) {
		return input.leftNode != null && input.rightNode != null;
	}

	void removeChildlessNode(TreeNode current, TreeNode parent, boolean leftNode) {
		if (current == root) {
			root = null;
		} else if (leftNode) {
			parent.leftNode = null;
		} else {
			parent.rightNode = null;
		}
	}

	void removeLeftChildNode(TreeNode current, TreeNode parent, boolean leftNode) {
		if (current == root) {
			root = root.leftNode;
		} else if (leftNode) {
			parent.leftNode = current.leftNode;
		} else {
			parent.rightNode = current.leftNode;
		}
	}

	void removeRightChildNode(TreeNode current, TreeNode parent, boolean leftNode) {
		if (current == root) {
			root = root.rightNode;
		} else if (leftNode) {
			parent.leftNode = current.rightNode;
		} else {
			parent.rightNode = current.rightNode;
		}
	}

	void removeTwoChildNode(TreeNode current, TreeNode parent, boolean leftNode) {
		TreeNode nextInOrder = nextInOrder(current);
		if (current == root) {
			root = nextInOrder;
		} else if (leftNode) {
			parent.leftNode = nextInOrder;
		} else {
			parent.rightNode = nextInOrder;
		}
		nextInOrder.leftNode = current.leftNode;
	}

	TreeNode nextInOrder(TreeNode input) {

		TreeNode current = input.rightNode;
		TreeNode inOrder = null;
		TreeNode inOrderParent = null;

		while (current != null) {
			inOrderParent = inOrder;
			inOrder = current;
			current = current.leftNode;
		}

		if (inOrder != input.rightNode) {
			inOrderParent.leftNode = inOrder.rightNode;
			inOrder.rightNode = input.rightNode;
		}

		return inOrder;
	}

	void addToArray(TreeNode input) {

		if (input == null) {
			return;
		}
		addToArray(input.leftNode);
		result.add(input.data);
		addToArray(input.rightNode);

	}

	public ArrayList<Integer> toArrayList() {
		result = new ArrayList<Integer>();
		addToArray(root);
		return result;
		// throw new UnsupportedOperationException();
	}
}